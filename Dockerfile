FROM python:3.8-alpine

COPY . /app
WORKDIR /app

RUN apk update && \
    apk add --no-cache --virtual=.build-deps gcc libffi-dev build-base postgresql-dev musl-dev && \
    apk add --no-cache --virtual=.run-deps postgresql-libs postgresql-client libxml2-dev libxslt-dev && \
    pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt && \
    apk --purge del .build-deps

CMD ["python", "manage.py", "runserver" ]
EXPOSE 5000
