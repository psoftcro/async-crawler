import os


DEBUG = os.environ.get("DEBUG", "1")
ENV = os.environ.get("ENV", "development")
HOST = os.environ.get("HOST", "0.0.0.0")
PORT = int(os.environ.get("PORT", 5000))
SECRET_KEY = os.environ.get("SECRET_KEY", "12345678")

DB_PORT = os.environ.get("DB_PORT", "5432")
DB_HOST = os.environ.get("DB_HOST", "postgres")
DB_USERNAME = os.environ.get("DB_USERNAME", "user")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "password")
DB_DATABASE = os.environ.get("DB_DATABASE", "CrawlerDB")
DB_DRIVER = os.environ.get("DB_DRIVER", "postgresql")

SWAGGER_UI_VERSION = os.environ.get("SWAGGER_UI_VERSION", "3")
SWAGGER_DOCS_VERSION = os.environ.get("SWAGGER_DOCS_VERSION", "0.1")
SWAGGER_TITLE = os.environ.get("SWAGGER_TITLE", "Async Crawler Api")
SWAGGER = {
    "title": f"{SWAGGER_TITLE} Documentation",
    "uiversion": SWAGGER_UI_VERSION,
    "openapi": "3.0.0",
    "info": {
        "description": None,
        "termsOfService": None,
        "title": SWAGGER_TITLE,
        "version": SWAGGER_DOCS_VERSION,
    },
    "headers": [],
    "specs": [
        {
            "endpoint": "apispec_1",
            "route": "/apispec_1.json",
            "rule_filter": lambda rule: True,
            "model_filter": lambda tag: True,
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/",
}

SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get(
    "SQLALCHEMY_TRACK_MODIFICATIONS", "false"
)
SQLALCHEMY_DATABASE_URI = (
    f"{DB_DRIVER}://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_DATABASE}"
)
