import requests
from bs4 import BeautifulSoup

from src.helpers.country_mapper import cm


def whois_parser(url):
    r = requests.get(f"https://whois.com/whois/{url}")
    soup = BeautifulSoup(r.text, "lxml")
    div = soup.find("pre", {"id": "registryData"})
    if not div:
        return None
    data = div.text.split("\r\n")
    for line in data:
        if len(line) < 1 or line.startswith("%"):
            continue
        line = line.split(":", 1)
        if len(line) == 1:
            line[0] = line[0].strip().upper()
            for key in cm:
                if (
                    key == line[0]
                    or cm[key]["country"].upper() in line[0]
                    or cm[key]["cctld"].upper()[1:] == line[0]
                ):
                    return key
        else:
            line[1] = line[1].strip().upper()
            for key in cm:
                if (
                    key == line[1]
                    or cm[key]["country"].upper() in line[1]
                    or cm[key]["cctld"].upper()[1:] == line[0]
                ):
                    return key
    return None


def domain_parser(url):
    domain = url.split(".")[-1]
    for key in cm:
        if domain in cm[key]["cctld"][1:]:
            return key
    return None
