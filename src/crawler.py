import aiohttp
import asyncio
from requests_html import HTML
from tldextract import extract


LIMITS = {1: 20, 2: 40}


class Crawler:
    def __init__(self, start_url, limit=25):
        self.start_url = start_url
        self.domain = extract(self.start_url).registered_domain
        self.depth_limit = len(LIMITS)
        self.visited_list = set()
        self.session = None
        self.semaphore = asyncio.BoundedSemaphore(limit)

    async def _create_session(self):
        self.session = aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False))

    async def _get_url_data(self, href):
        if not self.session:
            await self._create_session()

        print(f"Processing: {href}")
        async with self.semaphore:
            try:
                async with self.session.get(href, timeout=10) as response:
                    if "text/html" not in response.headers["Content-Type"]:
                        return None
                    text = await response.text()
                    html = HTML(url=href, html=text)
                    return html
            except Exception as e:
                if str(e):
                    print(f"Exception: {e}")

    def _get_absolute_links(self, data):
        url_list = []
        links = data.absolute_links

        for link in links:
            link = link.replace("://www.", "://")
            if link.endswith("/"):
                link = link[:-1]
            if (
                link not in self.visited_list
                and self.domain == extract(link).registered_domain
            ):
                url_list.append(link)

        return url_list

    async def _get_link(self, href):
        data = await self._get_url_data(href)
        url_list = set()

        if data:
            url_list = set(self._get_absolute_links(data))

        return data, sorted(url_list, key=len)

    async def _get_all_links(self, to_get):
        futures, results = [], []

        for href in to_get:
            if href in self.visited_list:
                continue

            self.visited_list.add(href)
            futures.append(self._get_link(href))

        for future in asyncio.as_completed(futures):
            try:
                results.append((await future))

            except Exception as e:
                if str(e):
                    print(f"Exception: {e}")

        return results

    def _process(self, data):
        raise NotImplementedError

    async def crawl(self):
        print(f"Started: {self.domain}")
        to_get = [self.start_url]
        results = {}

        for depth in range(self.depth_limit + 1):
            chunk = await self._get_all_links(to_get)
            to_get = []
            depth_limit = LIMITS.get(depth, 1)
            results[depth] = []
            for data, url_list in chunk:
                if data:
                    self._process(data)
                depth_urls = len(results[depth])

                if depth_urls == depth_limit:
                    break
                if data:
                    results[depth].append(data.url)
                to_get.extend(url_list)
                to_get = sorted(to_get, key=len)
        print(f"Finished: {self.domain}")
        await self.session.close()

        return results
