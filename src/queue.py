from aio_pika import connect, Message
from aio_pika.exceptions import QueueEmpty
from asyncio import get_event_loop


class RMQueue:
    def __init__(self):
        self.connection = None
        self.loop = get_event_loop()
        self.channel = None
        self.queue_name = "queue"
        self.queue = None
        self.routing_key = None
        self.exchange = None

    async def connect(self):
        self.connection = await connect(
            "amqp://rabbitmq:rabbitmq@rabbit1/", loop=self.loop
        )
        self.channel = await self.connection.channel()
        self.routing_key = self.queue_name
        self.exchange = await self.channel.declare_exchange("direct")
        self.queue = await self.channel.declare_queue(self.queue_name)
        await self.queue.bind(self.exchange, self.routing_key)

    async def disconnect(self):
        await self.queue.unbind(self.exchange, self.routing_key)
        await self.connection.close()
        self.connection = None

    async def push_message(self, msg):
        if not self.connection:
            await self.connect()
        await self.exchange.publish(
            Message(
                bytes(msg, "utf-8"),
                content_type="text/plain",
            ),
            self.routing_key,
        )
        print(f"Queue: added {msg} to queue")
        await self.disconnect()

    async def get_message(self):
        if not self.connection:
            await self.connect()
        try:
            incoming_message = await self.queue.get(timeout=5)
            incoming_message.ack()
            await self.disconnect()
            print(f"Queue: got {incoming_message.body.decode()} from queue")
            return incoming_message
        except QueueEmpty:
            await self.disconnect()
            return None


RabbitQueue = RMQueue()
