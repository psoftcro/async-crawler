from datetime import datetime

import asyncio
from flask import request, Blueprint
from tldextract import extract

from src.db.models import CrawledData
from src.modules.extensions import db
from src.modules.responses import get_response
from src.queue import RabbitQueue


bp = Blueprint("root", __name__, url_prefix="/")
loop = asyncio.get_event_loop()


@bp.route("/get/<string:domain>", methods=["GET"])
def get_domain(domain):
    crawled_data = db.session.query(CrawledData).get(domain)
    if not crawled_data:
        return get_response(404)
    return get_response(200, params=crawled_data.params)


@bp.route("/add", methods=["POST"])
def add_domain_for_crawling():
    data = request.json
    if not data.get("domain"):
        return get_response(400)
    domain = extract(data["domain"]).registered_domain
    if not domain:
        return get_response(400)

    db_domain = db.session.query(CrawledData).get(domain)
    if db_domain:
        if db_domain.status == "active":
            return get_response(
                200,
                params={
                    "domain": domain,
                    "status": "active",
                    "message": "already active",
                },
            )

        db_domain.status = "active"
        db_domain.updated_at = datetime.now()
        db.session.add(db_domain)
        db.session.commit()
        loop.run_until_complete(RabbitQueue.push_message(domain))
        return get_response(
            200,
            params={"domain": domain, "status": "active", "message": "started anew"},
        )

    crawled_data = CrawledData()
    crawled_data.domain = domain
    db.session.add(crawled_data)
    db.session.commit()
    loop.run_until_complete(RabbitQueue.push_message(domain))
    return get_response(
        201, params={"domain": domain, "status": "active", "message": "added"}
    )
