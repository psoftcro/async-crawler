from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base

from src.config import SQLALCHEMY_DATABASE_URI


metadata = MetaData()
Base = declarative_base(metadata=metadata)
engine = create_engine(SQLALCHEMY_DATABASE_URI)
