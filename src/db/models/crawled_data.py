from sqlalchemy import Column, DateTime, String
from sqlalchemy.dialects.postgresql import ARRAY, ENUM
from sqlalchemy.sql import func

from src.db.database import Base

status = {"active", "finished"}
status_enum = ENUM(*status, name="status")


class CrawledData(Base):
    __tablename__ = "crawled_data"

    domain = Column(String(255), primary_key=True)
    language = Column(String(2), nullable=True)
    country = Column(String(2), nullable=True)
    phone_numbers = Column(ARRAY(String(255)))
    emails = Column(ARRAY(String(255)))
    created_at = Column(DateTime, default=func.now(), server_default=func.now())
    updated_at = Column(DateTime, default=func.now(), server_default=func.now())
    status = Column(
        status_enum, nullable=False, default="active", server_default="active"
    )

    def __repr__(self):
        return f"<{self.__class__.__name__}(domain={self.domain})>"

    @property
    def params(self):
        return {
            "domain": self.domain,
            "language": self.language,
            "country": self.country,
            "phone_numbers": self.phone_numbers,
            "emails": self.emails,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "status": self.status,
        }
