from re import findall, compile

from langua import Predict
from phonenumbers import format_number, PhoneNumberFormat, PhoneNumberMatcher

from src.crawler import Crawler
from src.helpers.country import whois_parser, domain_parser


class DataProcessor(Crawler):
    def __init__(self, start_url, limit=25):
        self.emails = []
        self.phone_numbers = []
        self.country = None
        self.language = None
        super().__init__(start_url, limit)

    def results(self):
        return {
            "emails": self.emails,
            "language": self.language,
            "country": self.country,
            "phone_numbers": self.phone_numbers,
        }

    def _process(self, data):
        if data.url == self.start_url:
            self._process_language(data)
            self._process_country(data)
        self._process_emails(data)
        if self.country:
            self._process_phone_numbers(data)

    def _process_emails(self, data):
        regex = compile(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")
        self.emails = list(set(self.emails + findall(regex, data.text)))

    def _process_country(self, data):
        self.country = (
            whois_parser(data.url)
            if whois_parser(data.url)
            else domain_parser(data.url)
        )

    def _process_language(self, data):
        pq = data.pq
        pq("script").remove()
        language = Predict()
        self.language = language.get_lang(pq.text())

    def _process_phone_numbers(self, data):
        matches = PhoneNumberMatcher(data.text, self.country)
        for match in matches:
            self.phone_numbers.append(
                format_number(match.number, PhoneNumberFormat.INTERNATIONAL)
            )
        self.phone_numbers = list(set(self.phone_numbers))
