from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flasgger import Swagger

from src.db.database import metadata


swagger = Swagger(template_file="docs/docs.yaml")
db = SQLAlchemy(metadata=metadata)
migrate = Migrate()


def configure_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    swagger.init_app(app)
