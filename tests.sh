#!/bin/bash
docker-compose up &>/dev/null &
echo "Running tests..."
while ! docker exec crawler_db sh &>/dev/null ; do
  sleep 0.1
done
docker exec -it crawler_api sh -c "apk add gcc libffi-dev build-base postgresql-dev musl-dev &>/dev/null"
if [ $# -eq 1 ]; then
  command="pytest tests/integration/$1"
else
  command="tox -e integration_tests"
fi
docker exec -it crawler_api sh -c "$command"
docker-compose stop &>/dev/null &