from flask_migrate import MigrateCommand
from flask_script import Manager, Server
from waitress import serve

from src.config import ENV, HOST, PORT
from src.app import app


manager = Manager(app)
server = (
    serve(app, host=HOST, port=PORT)
    if ENV == "production"
    else Server(host=HOST, port=PORT)
)
manager.add_command("runserver", server)
manager.add_command("db", MigrateCommand)


if __name__ == "__main__":
    manager.run()
