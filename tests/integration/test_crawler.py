from unittest.mock import patch

from asyncio import Future

from src.db.models import CrawledData


def test_get_domain(db, client):
    with db.context():
        response = client.get(f"/get/test.com")
        data, code = response.json, response.status_code
        assert code == 200


def test_get_invalid_domain(db, client):
    with db.context():
        response = client.get(f"/get/test2.com")
        data, code = response.json, response.status_code
        assert code == 404


@patch("src.routes.RabbitQueue.push_message")
def test_add_domain(mock_queue, db, client):
    with db.context():
        domain = "index.hr"
        mock_queue.return_value = Future()
        mock_queue.return_value.set_result(True)
        headers = {"Content-Type": "application/json"}
        data = {"domain": domain}
        response = client.post(f"/add", json=data, headers=headers)
        data, code = response.json, response.status_code
        db_model = db.session.query(CrawledData).get(domain)
        assert (
            code == 201 and db_model.domain == domain and data.get("message") == "added"
        )


@patch("src.routes.RabbitQueue.push_message")
def test_recrawl_domain(mock_queue, db, client):
    with db.context():
        domain = "test.com"
        mock_queue.return_value = Future()
        mock_queue.return_value.set_result(True)
        headers = {"Content-Type": "application/json"}
        data = {"domain": domain}
        response = client.post(f"/add", json=data, headers=headers)
        data, code = response.json, response.status_code
        db_model = db.session.query(CrawledData).get(domain)
        assert (
            code == 200
            and db_model.domain == domain
            and data.get("message") == "started anew"
        )


def test_active_domain(db, client):
    with db.context():
        domain = "example.com"
        headers = {"Content-Type": "application/json"}
        data = {"domain": domain}
        response = client.post(f"/add", json=data, headers=headers)
        data, code = response.json, response.status_code
        db_model = db.session.query(CrawledData).get(domain)
        assert (
            code == 200
            and db_model.domain == domain
            and data.get("message") == "already active"
        )


def test_add_invalid_domain(db, client):
    with db.context():
        domain = "indexhr"
        headers = {"Content-Type": "application/json"}
        data = {"domain": domain}
        response = client.post(f"/add", json=data, headers=headers)
        data, code = response.json, response.status_code
        db_model = db.session.query(CrawledData).get(domain)
        assert code == 400 and db_model is None
