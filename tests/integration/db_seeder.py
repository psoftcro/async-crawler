from src.db.models import CrawledData


def seed(session):
    crawled_data = [
        CrawledData(
            domain="test.com",
            language="hr",
            country="hr",
            phone_numbers=[
                "+385 1 2345 678",
                "+385 1 7215 220",
                "+385 1 4321 538",
                "+385 1 3124 559",
            ],
            emails=["test@test.com"],
            status="finished",
        ),
        CrawledData(
            domain="example.com",
            language="en",
            country="gb",
            phone_numbers=["+44 1 2345 608"],
            emails=["example@example.com"],
            status="active",
        ),
    ]
    session.add_all(crawled_data)
    session.commit()
