from datetime import datetime

import asyncio
from sqlalchemy.orm import Session

from src.data_processor import DataProcessor
from src.db.database import engine
from src.db.models import CrawledData
from src.queue import RabbitQueue


async def main():
    await asyncio.sleep(20)
    message = await RabbitQueue.get_message()
    if message:
        domain = message.body.decode()
        url = f"https://{domain}"
        dp = DataProcessor(url)
        await dp.crawl()
        crawled_results = dp.results()
        session = Session(engine)
        db_domain = session.query(CrawledData).get(domain)
        db_domain.emails = crawled_results["emails"]
        db_domain.language = crawled_results["language"]
        db_domain.country = crawled_results["country"]
        db_domain.phone_numbers = crawled_results["phone_numbers"]
        db_domain.updated_at = datetime.now()
        db_domain.status = "finished"
        session.add(db_domain)
        session.commit()
        session.close()
    else:
        await asyncio.sleep(10)
    return await main()


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
